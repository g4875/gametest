## Getting Started
This program runs on windows and linux, for windows Visual Studio 2019 is recuired, this program is untested on other windows development environments and might not be able to work without Visual Studio 2019.

<ins>**1. Downloading the repository:**</ins>

Start by cloning the repository with `git clone --recursive https://gitlab.com/g4875/gametest.git`.

If the repository was cloned non-recursively previously, use `git submodule update --init` to clone the necessary submodules.

<ins>**2. Generating project:**</ins>

On Linux run the shell script named `GenerateLinuxProgram.sh`
On Windows run the bat file named  `GenerateWindowsProject.bat`

<ins>**3. Building:**</ins>

On linux run `make`in the projects root folder. and then to run the program run the executable under `bin/Debug/OpenGLBoilerPlate`

On Windows build the project in Visual Studio and after that you should be able to run it through Visual Studio.
